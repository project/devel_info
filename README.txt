

Module
======
Devel info


Description
===========
Displays useful and helpful information for the module and theme development.

This module provides:
* General information
  A block which displays some general information.
* Module information
  A block which displays a list of enabled modules.
* Theme information
  A block which displays a list of enabled themes.


Requirements
============
Drupal 7.x


Installation
============
1. Move this folder into your modules directory.
2. Enable it from Administer >> Modules >> Development.


Configuration
=============
Enable and configure blocks at >> Administer >> Structure >> Blocks.


Projekt page
============
http://drupal.org/project/devel_info


Author & maintainer
===================
Ralf Stamm


License
=======
GNU General Public License (GPL)
